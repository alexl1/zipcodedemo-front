export default class Error {
    constructor(err, errTime) {
        this.errLiveTime = errTime || 5000;
        this.errID = 'err_' + Date.now();
        this.showErr(err);
    }

    /**
     * Shows error and call hide method
     * @param err
     */
    showErr(err) {
        const errWrapper = document.createElement('div');
        const errTitle = document.createElement('p');
        const errContent = document.createElement('p');

        errWrapper.classList.add('err');
        errWrapper.setAttribute('id', this.errID);

        errTitle.classList.add('err__title');
        errTitle.innerText = 'Error';

        errContent.classList.add('err_content');
        errContent.innerText = JSON.stringify(err);

        errWrapper.appendChild(errTitle);
        errWrapper.appendChild(errContent);
        document.body.appendChild(errWrapper);
        this.hideError();
    }

    /**
     * Hides error through time that specified in errLiveTime
     */
    hideError() {
        setTimeout(() => document.getElementById(this.errID).remove(), this.errLiveTime);
    }
}
