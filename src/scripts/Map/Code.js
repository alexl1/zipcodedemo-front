export default class Code {
    constructor(map, code) {
        this.map = map;
        /**
         * An object with code info:
         * {
         *      center: Array<latLng>
         *      code: Number
         *      createdAt: Date
         *      geometry: String
         *      lat: Number
         *      lng: Number
         *      state: String
         *      _id: String
         * }
         */
        this.code = code;
        this.listEl = document.getElementById('codesList');
        this.codeOnList = null;
        this.codeOnMap = null;
        this.addCodeOnMap();
        this.addCodeToTheList();
    }

    /**
     * Gets code and return it
     * @returns {number}
     */
    getCode() {
        return this.code.code;
    }

    /**
     * Adds code to the map
     */
    addCodeOnMap() {
        if ( this.code.geometry ) {
            /**
             * Parses geometry string and make an array with objects, that consist lat and lng properties
             * @type {object}
             */
            const geometry = this.code.geometry
                .split(';')[0]
                .split(' ')
                .map(cords => cords
                    .split(',')
                    .map(cord => parseFloat(cord))
                    .splice(0, 2))
                .map(cord => {
                    return {
                        lat: cord[1],
                        lng: cord[0]
                    };
                });
            /**
             * Create new shape
             * @type {google.maps.Polygon}
             */
            const shape = new google.maps.Polygon({
                paths: geometry,
                strokeColor: '#FF0000',
                strokeOpacity: 0.6,
                strokeWeight: 1,
                fillColor: '#fff80b',
                fillOpacity: 0.25
            });
            /**
             * String with content that will be displayed on infoWindow of polygon
             * @type {string}
             */
            const contentString = '<div id="content">'+
                '<p style="text-align: center">Code info</p>' +
                '<ul>' +
                '<li>Zip code: <b>' + this.code.code +'</b></li>' +
                '<li>State: <b>' + this.code.state + '</b></li>' +
                '</ul>' +
                '</div>';
            /**
             * Create new infoWindow
             * @type {google.maps.InfoWindow}
             */
            const infoWindow = new google.maps.InfoWindow({
                content: contentString
            });
            shape.setMap(this.map);

            /**
             * Adds click event on code shape for showing infoWindow
             */
            google.maps.event.addListener(shape, 'click', event => {
                infoWindow.setPosition(event.latLng);
                infoWindow.open(this.map);
            });
            /**
             * Adds listener for close infoWindow by click on map
             */
            google.maps.event.addListener(this.map, 'click', () => infoWindow.close());
            this.codeOnMap = shape;
        }
    }

    /**
     * Adds code to the list
     */
    addCodeToTheList() {
        const li = document.createElement('li');
        li.innerText = this.code.code;
        this.listEl.appendChild(li);
        this.codeOnList = li;
    }

    /**
     * Deletes code from list and map
     */
    deleteCode() {
        if ( this.codeOnMap ) this.codeOnMap.setMap(null);
        if ( this.codeOnList ) this.codeOnList.remove();
    }
}
