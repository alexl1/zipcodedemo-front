import Code from './Code';
import Error from '../Error';

export default class DrawingManager {
    constructor(map) {
        this.map = map;
        this.drawingManager = null;
        /**
         * property codes contain array of codes that was gotten from APi
         * the codes must contain instances of the class Code
         */
        this.codes = [];
        /**
         * drawingActive contain current status of drawing manager active or no
         */
        this.drawingActive = false;
        this.drawBtn = document.getElementById('areaBtn');
        this.clearBtn = document.getElementById('clearBtn');
        this.initDrawingManager();
    }

    /**
     * Inits Google Maps Drawing Manager
     */
    initDrawingManager() {
        this.drawingManager = new google.maps.drawing.DrawingManager({
            drawingMode: null,
            drawingControl: false,
            polygonOptions: {
                fillColor: '#ff0b00',
                fillOpacity: .3,
                strokeWeight: 2,
                clickable: false,
                editable: false,
                draggable: false,
                zIndex: 1
            }
        });
        this.drawingManager.setMap(this.map);

        this.initListeners();
    }

    /**
     * Inits listeners for Google Maps and drawn and clear buttons
     */
    initListeners() {
        google.maps.event.addListener(this.drawingManager, 'polygoncomplete', event => {
            if ( event.getPath().length < 3 ) {
                event.setMap(null);
                return new Error('Polygon should have at least 3 points');
            }
            /**
             * event.getPath() return MVCArray from Google Maps APi
             * we uses getArray() method to generate array from MVCArray and map to fill it by lat/lng coords
             */
            const polygonBounds = event.getPath().getArray().map(point => {
                return `${point.lat()},${point.lng()}`;
            });
            /**
             * Removes polygon boundary from map
             */
            event.setMap(null);
            this.getCodes(polygonBounds.join(';'));
        });
        this.drawBtn.addEventListener('click', () => {
            if ( this.drawingActive ) {
                this.drawingManager.setOptions({
                    drawingMode: null
                });
                this.drawingActive = false;
                this.drawBtn.classList.remove('active');
            } else {
                this.drawingManager.setOptions({
                    drawingMode: google.maps.drawing.OverlayType.POLYGON
                });
                this.drawingActive = true;
                this.drawBtn.classList.add('active');
            }
        });
        this.clearBtn.addEventListener('click', () => {
            /**
             * Uses class Code method to delete code from list and map
             */
            this.codes.forEach( code => code.deleteCode());
            this.codes = [];
        });
    }

    /**
     * Makes request to the APi and gets code who fall into the polygon
     * @param polyGeom
     */
    getCodes(polyGeom) {
        const { protocol, hostname } = window.location;

        fetch(`${protocol}//${hostname}:3009/codes?cords=${polyGeom}`, {
            method: 'GET'
        })
            .then(res => res.json())
            .then(res => {
                /**
                 * Gets response object that contain object:
                 * {
                 *     status: Boolean,
                 *     message: String,
                 *     err: Object,
                 *     data: Array
                 * }
                 */
                if ( res.status ) {
                    res.data.forEach(code => {
                        /**
                         * Checks if the current code is not in the array then adds it
                         */
                        if ( !this.codes.find( _code => _code.getCode() === code.code ) )
                            this.codes.push(new Code(this.map, code));
                    });
                } else {
                    /**
                     * Creates a new instance of Error class and show error with res.err text
                     */
                    new Error(res.err);
                    console.error(res.err);
                }
            });
    }
}
