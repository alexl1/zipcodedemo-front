import 'whatwg-fetch';
import '../styles/fonts.scss';
import '../styles/index.scss';
import Map from './Map/Map';

function initMap() {
    new Map();
}
window.initMap = initMap;
